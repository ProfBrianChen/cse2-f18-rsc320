/// By Rakene Chowdhury
/// Due on 09/04/2018
/// CSE 002 Section 210
/// Welcome message script that prints out a decorative message that introduces me.
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints the decorative message to terminal window
    System.out.println("   -----------");
    System.out.println("   | WELCOME |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-R--S--C--3--2--0->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    //prints an empty line
    System.out.println(" ");
    //prints my autobiographic tweet
    System.out.println("Hey, I'm Rakene, a junior that just switched to studying CSE. I enjoy playing soccer, jamming with my friends, and going fishing.");                   
    
  }
  
}