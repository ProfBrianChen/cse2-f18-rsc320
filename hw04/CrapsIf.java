/// By Rakene Chowdhury
/// Due on 09/25/2018
/// CSE 002 Section 210
/// This is a script that produces the craps slang for a dice roll that is given or random
//
/// Importing the Scanner object
import java.util.Scanner;
//Creating a class called CrapsIf
public class CrapsIf {
  // main method required for every Java program
  public static void main (String[] args) {
    //Variables
    int dice1 = 0;
    int dice2 = 0;
    int sum;
    // Declaring an instance of the Scanner object
    Scanner myScanner = new Scanner(System.in);
    //Prompting the user to choose to give dice rolls or have them randomly choosen
    System.out.print("Would you like to state the two dice?  If not it will be randomly generated. (Yes/No):");
    String random = myScanner.next();
    //If statement to choose random generation or given dice
    if(random.equals("Yes")){
      //Ask for the dice states
      System.out.print("What is roll of the first dice? (An integer 1-6)");
      dice1 = myScanner.nextInt();
      System.out.print("What is roll of the second dice? (An integer 1-6)");
      dice2 = myScanner.nextInt();
      //Checking to make sure the dice states are integers 1-6
      if(dice1 >=1 && dice1 <= 6){
        if(dice2 >=1 && dice2 <= 6){
        } else {
          System.out.println("Error:Entry not an integer 1-6");
        }
      } else{
        System.out.println("Error:Entry not an integer 1-6");
      }
      //Randomly generating the dice
    } else if(random.equals("No")){
      dice1 = (int)(Math.random()*6)+1;
      dice2 = (int)(Math.random()*6)+1;
      System.out.println("Your rolls are " + dice1 + " and " + dice2);
    } else {
      System.out.println("Error:Entry not Yes or No");
    }
    //Creating the sum of the dice for sorting purposes
    sum = dice1 + dice2;
    //Giant if statement to assign the correct name
    if(sum == 2){
      System.out.println("You rolled Snake Eyes");
    } else if(sum == 3){
      System.out.println("You rolled an Ace Deuce");
    } else if(sum == 4){
      if(dice1 == 2 && dice2 == 2){
        System.out.println("You rolled a Hard Four");
      } else{
        System.out.println("You rolled a Easy Four");
      }
    } else if(sum == 5){
      System.out.println("You rolled a Fever Five");
    } else if(sum == 6){
      if(dice1 == 3 && dice2 == 3){
        System.out.println("You rolled a Hard Six");
      } else{
        System.out.println("You rolled a Easy Six");
      }
    } else if(sum == 7){
      System.out.println("You rolled a Seven Out");
    } else if(sum == 8){
      if(dice1 == 4 && dice2 == 4){
        System.out.println("You rolled a Hard Eight");
      } else{
        System.out.println("You rolled a Easy Eight");
      }
    } else if(sum == 9){
      System.out.println("You rolled a Nine");
    } else if(sum == 10){
      if(dice1 == 5 && dice2 == 5){
        System.out.println("You rolled a Hard Ten");
      } else{
        System.out.println("You rolled a Easy Ten");
      }
    } else if(sum == 11){
      System.out.println("You rolled a Yo-leven");
    } else if(sum == 12){
      System.out.println("You rolled Boxcars");
    } else{
      System.out.println("Error: Could not find any slang for that roll");
    }
  }
}