/// By Rakene Chowdhury
/// Due on 10/03/2018
/// CSE 002 Section 210
/// This is a script that takes input about the user's class and checks the input types.
//
//Importing the Scanner Class
import java.util.Scanner;

//Creating a class called CardGenerator
public class lab05 {
  // main method required for every Java program
  public static void main (String[] args) {
    // initializing scanner object
    Scanner read = new Scanner(System.in);
    //defining the variables
    int courseNum;
    String depName;
    int studentNum;
    int classTime;
    String instructorName;
    int meetWeek;
    String junk;
    //prompting user for different info
    System.out.println("Please answer some questions about a current class you are enrolled in.");
    System.out.println("What is the course number of the class?");
    //Checks that the user response is a int and if not asks for another input
    while (!read.hasNextInt()) {
      System.out.println("Error: Wrong input type");
      junk = read.nextLine();
    }
    // using scanner object to read the input to a variable
    courseNum = read.nextInt();
    junk = read.nextLine();
    //prompting user for different info
    System.out.println("What is the name of the department that the class is in?");
    // using scanner object to read the input to a variable
    depName = read.nextLine();
    //prompting user for different info
    System.out.println("How many students are in your class?");
    //Checks that the user response is a int and if not asks for another input
    while (!read.hasNextInt()) {
      System.out.println("Error: Wrong input type");
      junk = read.nextLine();
    }
    // using scanner object to read the input to a variable
    studentNum = read.nextInt();
    junk = read.nextLine();
    //prompting user for different info
    System.out.println("What time does the class meet in military time?");
    //Checks that the user response is a int and if not asks for another input
    do { if (read.hasNextInt()) {
      break;
    } else {
      System.out.println("Error: Wrong input type");
      junk = read.nextLine();
    }
       } while (true);
    // using scanner object to read the input to a variable
    classTime = read.nextInt();
    junk = read.nextLine();
    //prompting user for different info
    System.out.println("What is the name of the instructor of the class?");
    // using scanner object to read the input to a variable
    instructorName = read.nextLine();
    //prompting user for different info
    System.out.println("How many times does the class meet per week?");
    //Checks that the user response is a int and if not asks for another input
    do { if (read.hasNextInt()) {
      break;
    } else {
      System.out.println("Error: Wrong input type");
      junk = read.nextLine();
    }
       } while (true);
    // using scanner object to read the input to a variable
    meetWeek = read.nextInt();
    junk = read.nextLine();
    //Printing out all the data we have collected int he script
    System.out.println("Course Number: " + courseNum);
    System.out.println("Department Name:" + depName);
    System.out.println("Number of Students: " + studentNum);
    System.out.println("Class Time: " + classTime);
    System.out.println("Instructor Name: " + instructorName);
    System.out.println("Meetings per Week: " + meetWeek);
  }
}