/// By Rakene Chowdhury
/// Due on 09/25/2018
/// CSE 002 Section 210
/// This is a script that produces the craps slang for a dice roll that is given or random
//
/// Importing the Scanner object
import java.util.Scanner;
//Creating a class called CrapsSwitch
public class CrapsSwitch {
  // main method required for every Java program
  public static void main (String[] args) {
    //Variables
    int dice1 = 0;
    int dice2 = 0;
    int sum;
    // Declaring an instance of the Scanner object
    Scanner myScanner = new Scanner(System.in);
    //Prompting the user to choose to give dice rolls or have them randomly choosen
    System.out.print("Would you like to state the two dice?  If not it will be randomly generated. (Yes/No):");
    String random = myScanner.next();
    //Switch statement to choose random generation or given dice
    switch (random){
      case "Yes":
        //Ask for the dice states
        System.out.print("What is roll of the first dice? (An integer 1-6)");
        dice1 = myScanner.nextInt();
        System.out.print("What is roll of the second dice? (An integer 1-6)");
        dice2 = myScanner.nextInt();
        //I know there is an easier way to just have several cases but I made it using one case
        //created double variables of my dice to create a sorting variable
        double dice11 = dice1;
        double dice22 = dice2;
        //Made an equation that when truncated by casting to an Integer only gives the range of 1-6 as 1
        double sort1 = Math.sqrt((dice11+1)/2);
        double sort2 = Math.sqrt((dice11+1)/2);
        //Checking if the entry is an integer 1-6
        switch ((int)sort1){
          case 1:
            break;
          default:
            System.out.println("Error: Entry was not an integer 1-6");
            break;
        }
        switch ((int)sort2){
          case 1:
            break;
          default:
            System.out.println("Error: Entry was not an integer 1-6");
            break;
        }
        break;
        // making the random dice rolls
      case "No":
        dice1 = (int)(Math.random()*6)+1;
        dice2 = (int)(Math.random()*6)+1;
        System.out.println("Your rolls are " + dice1 + " and " + dice2);
        break;
      default:
        System.out.println("Error: Entry not Yes or No");
        break;
    }
    //creates sum of the dice as a sorting mechanism
    sum = dice1 + dice2;
    //sorts the dice rolls to their names
    switch (sum){
      case 2:
        System.out.println("You rolled Snake Eyes");
        break;
      case 3:
        System.out.println("You rolled an Ace Deuce");
        break;
      case 4:
        switch (dice1){
          case 2:
            System.out.println("You rolled a Hard Four");
            break;
          default:
            System.out.println("You rolled a Easy Four");
            break;
        }
        break;
      case 5:
        System.out.println("You rolled a Fever Five");
        break;
      case 6:
        switch (dice1){
          case 3:
            System.out.println("You rolled a Hard Six");
            break;
          default:
            System.out.println("You rolled a Easy Six");
            break;
        }
        break;
      case 7:
        System.out.println("You rolled a Seven Out");
        break;
      case 8:
        switch (dice1){
          case 4:
            System.out.println("You rolled a Hard Eight");
            break;
          default:
            System.out.println("You rolled a Easy Eight");
            break;
        }
        break;
      case 9:
        System.out.println("You rolled a Nine");
        break;
      case 10:
        switch (dice1){
          case 5:
            System.out.println("You rolled a Hard Ten");
            break;
          default:
            System.out.println("You rolled a Easy Ten");
            break;
        }
        break;
      case 11:
        System.out.println("You rolled a Yo-leven");
        break;
      case 12:
        System.out.println("You rolled Box Cars");
        break;
      default:
        System.out.println("Error: Could not find any slang for that roll");
        break;
    }
  }
}