/// By Rakene Chowdhury
/// Due on 09/18/2018
/// CSE 002 Section 210
/// A program that asks the user for doubles 
///that represent the number of acres of land affected by hurricane precipitation and 
///how many inches of rain were dropped on average.  Convert the quantity of rain into cubic miles.
///

/// Importing the Scanner object
import java.util.Scanner;

/// Creates public class
public class Convert{
  //main method required by every java class
  public static void main(String args[]){
    // Declaring an instance of the Scanner object
    Scanner myScanner = new Scanner(System.in);
    
    //Prompting user to submit the acres
    System.out.print("Enter the affected areas in acres: ");
    double acres = myScanner.nextDouble();
    
    //Prompting user to submit the average rainfall
    System.out.print("Enter the rainfall in the affected areas: ");
    double rainFall = myScanner.nextDouble();
    
    //Calculating cubicMiles
    double cubicMiles = acres * rainFall * 27154.2857 * 9.08169e-13;
    
    //Printing out the results
    System.out.print(cubicMiles + " cubic miles");
  }
}