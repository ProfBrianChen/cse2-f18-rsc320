/// By Rakene Chowdhury, Austin Leopard, and Ngan Tran
/// Due on 09/04/2018
/// CSE 002 Section 210
/// Setting up a transaction log for the last month of venmo payments
///
import java.util.Scanner;
public class ReportWhile{
  public static void main (String[] args){
    int numTran;
    int counter = 0;
    double moneyTotal = 0;
    double moneyHigh = 0;
    double avgTran = 0;
    double tranCost=0;
    Scanner myScanner = new Scanner(System.in);
    System.out.println("How many Venmo transactions have you sent to others in this last month of September?");
    numTran = myScanner.nextInt();
    while (counter < numTran) {
      counter = counter + 1;
      System.out.println("How much money was your " + counter + " transaction of the month? (Double Form)");
      tranCost = myScanner.nextDouble();
      moneyTotal = moneyTotal + tranCost;
      if (tranCost > moneyHigh) {
        moneyHigh = tranCost;
      }
    }
    avgTran = moneyTotal / counter;
    System.out.println("The total value of money is " + moneyTotal);
    System.out.println("The highest transaction amount is " + moneyHigh);
    System.out.println("The average transaction value is " + avgTran);
    
  }
}