/// By Rakene Chowdhury
/// Due on 09/05/2018
/// CSE 002 Section 210
/// Welcome message script that prints out a decorative message that introduces me.
//
///My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds, 
//and the number of rotations of the front wheel during that time. 
//For two trips, given time and rotation count, your program should
//print the number of minutes for each trip
//print the number of counts for each trip
//print the distance of each trip in miles
//print the distance for the two trips combined
//

//Creating a class called Cyclometer
public class Cyclometer {
  // main method required for every Java program
  public static void main (String[] args) {
    //Input data
    int secsTrip1 = 480;
    int secsTrip2 = 3220;
    int countsTrip1 = 1561;
    int countsTrip2 = 9037;
    //Defining variables
    double wheelDiameter = 27.0;
    double PI = 3.14159;
    int feetPerMile = 5280;
    int inchesPerFoot = 12;
    int secondsPerMinute = 60;
    double distanceTrip1, distanceTrip2, totalDistance;
    //Printing out basic data
    System.out.println("Trip 1 took " + ((double)secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + ((double)secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    //Calculating the distance
    distanceTrip1 = countsTrip1 * wheelDiameter * PI / (inchesPerFoot * feetPerMile);
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / (inchesPerFoot * feetPerMile);
    totalDistance = distanceTrip1 + distanceTrip2;
    //Printing out the distances
    System.out.println("Trip 1 was " + distanceTrip1 + "miles");
    System.out.println("Trip 2 was " + distanceTrip2 + "miles");
    System.out.println("The total distance was " + totalDistance + "miles");
    
    
  }
}