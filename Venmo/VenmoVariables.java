/// By Rakene Chowdhury, Austin Leopard, and Ngan Tran
/// Due on 09/04/2018
/// CSE 002 Section 210
/// Setting up variables for our Venmo clone.
///
public class VenmoVariables{
  public static void main(String args[]){
    //userId represents the Username
    String userId = "Username";
    //password holds the user's password
    String password = "Password";
    System.out.println("The user's login credentials are " + userId + " and " + password);
    //phoneNum holds the users phone number
    int phoneNum = 5555555;
    System.out.println("The user's phone number is " + phoneNum);
    //routingNum holds the user's routing number to his/her bank account
    int routingNum = 100000000;
    //accountNum holds the user's account number to his/her bank account
    int accountNum = 100000000;
    System.out.println("The user's bank account has the account number, " + accountNum + " and the routing number, " + routingNum);
    //cardNum represents the user's credit or debit card number
    String cardNum = "111111111111";
    //exp represents the date that the user's credit/debit card expires
    int exp = 1231;
    // csv represents the csv number on the back of the user's credit/debit card
    int csv = 123;
    System.out.println("The user's credit or debit card number is " + cardNum + " with an experation of " + exp + " and a csv of " + csv);
    //Mulitple variables to collect billing address
    String strAddress = "4 Farrington Square";
    String city = "Bethlehem";
    String state = "PA";
    String zipcode = "18015";
    System.out.println("The user's address is " + strAddress  + ", " + city + ", " + state + " " + zipcode);
    //email represents the the email address of the user
    String email = "user@gmail.com";
    System.out.println("The user's email is " + email);
    //transctCall represents the ammount of money charged or payed to another user
    double transactCall = 00.01;
    System.out.println("The user requests $" + transactCall + " in a transaction.");
  }
}