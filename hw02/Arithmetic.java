/// By Rakene Chowdhury
/// Due on 09/11/2018
/// CSE 002 Section 210
/// A script that adds subtotals of a reciept and calculates the tax and total cost
///
public class Arithmetic{
  
  public static void main(String args[]){
    // Define relevant variables
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    //Total cost of pants pretax
    double totalCostPant;
    //Total cost of sweatshirts pretax
    double totalCostSweatshirt;
    //Total cost of belts pretax
    double totalCostBelt;
    //Sales tax charged on Pants
    double salesTaxPant;
    //Sales tax charged on Sweatshirts
    double salesTaxSweatshirt;
    //Sales tax charged on Belts
    double salesTaxBelt;
    //Total cost of purchases before tax
    double totalCostNoTax;
    //Total sales tax charged
    double totalSalesTax;
    //Total paid for this transaction, including sales tax
    double totalCost;
    
    //Calculate my variables values
    totalCostPant = numPants * pantsPrice;
    totalCostSweatshirt = numShirts * shirtPrice;
    totalCostBelt = numBelts * beltPrice;
    salesTaxPant = paSalesTax * totalCostPant;
    salesTaxSweatshirt = paSalesTax * totalCostSweatshirt;
    salesTaxBelt = paSalesTax * totalCostBelt;
    totalCostNoTax = totalCostPant + totalCostSweatshirt + totalCostBelt;
    totalSalesTax = paSalesTax * totalCostNoTax;
    totalCost = totalCostNoTax + totalSalesTax;
    
    //Changing the variable to only have 2 decimal points
    salesTaxPant = (int)(salesTaxPant * 100);
    salesTaxPant = (double)salesTaxPant / 100;  
    salesTaxBelt = (int)(salesTaxBelt * 100);
    salesTaxBelt = (double)salesTaxBelt / 100;  
    salesTaxSweatshirt = (int)(salesTaxSweatshirt * 100);
    salesTaxSweatshirt = (double)salesTaxSweatshirt / 100;  
    totalSalesTax = (int)(totalSalesTax * 100);
    totalSalesTax = (double)totalSalesTax / 100;  
    totalCost = (int)(totalCost * 100);
    totalCost = (double)totalCost / 100; 
    
    //Print out all my computed variables
    System.out.println("The total cost of all the pants before tax is $" + totalCostPant + " and the tax paid for that is $" + salesTaxPant + ".");
    System.out.println("The total cost of all the sweatshirts before tax is $" + totalCostSweatshirt + " and the tax paid for that is $" + salesTaxSweatshirt + ".");
    System.out.println("The total cost of all the belts before tax is $" + totalCostBelt + " and the tax paid for that is $" + salesTaxBelt + ".");
    System.out.println("The total cost of all the items befor tax is $" + totalCostNoTax + " and the tax paid for that is $" + totalSalesTax + ".");
    System.out.println("The total cost of all the items including tax is $" + totalCost + ".");
  }
}