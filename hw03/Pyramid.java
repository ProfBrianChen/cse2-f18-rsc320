/// By Rakene Chowdhury
/// Due on 09/18/2018
/// CSE 002 Section 210
/// A program that takes inputs of the dimensions of a pyramid and outputs the volume of the pyramid
/// Importing the Scanner object
import java.util.Scanner;

/// Creates public class
public class Pyramid{
  //main method required by every java class
  public static void main(String args[]){
    // Declaring an instance of the Scanner object
    Scanner myScanner = new Scanner(System.in);
    
    //Prompting user to submit the length of the pyramid
    System.out.print("Enter the length of the square side of the pyramid: ");
    double length = myScanner.nextDouble();
    
    //Prompting user to submit the height of the pyramid
    System.out.print("Enter the height of the pyramid: ");
    double height = myScanner.nextDouble();
    
    //Calculating the volume
    double volume = (Math.pow(length, 2.0) * height) / 3.0;
    
    //Printing the results
    System.out.print("The volume of the pyramid is " + volume);
  }
}
