/// By Rakene Chowdhury
/// Due on 09/03/2018
/// CSE 002 Section 210
/// Simple script that prints out the message "Hello, World".
///
public class HelloWorld{
  
  public static void main(String args[]){
    ///prints Hello, World to terminal window
    System.out.println("Hello, World");
    
  }
  
}