/// By Rakene Chowdhury
/// Due on 09/12/2018
/// CSE 002 Section 210
/// This is a script that uses the Scanner class to obtain from the user the original cost of the check, 
/// the percentage tip they wish to pay, and the number of ways the check will be split. 
/// Then determine how much each person in the group needs to spend in order to pay the check.
//

/// Importing the Scanner object
import java.util.Scanner;

//Creating a class called Cyclometer
public class Check {
  // main method required for every Java program
  public static void main (String[] args) {
    // Declaring an instance of the Scanner object
    Scanner myScanner = new Scanner(System.in);
    
    //Prompting user to submit the cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    //Prompting user to submit the desired tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    //Converting the tip to a decimal
    tipPercent /= 100;
    
    //Prompt the user to submit the number of people
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //Initializing some variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    //Calculating the variables
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //Converting costPerPerson to dollars, dimes, and pennies for printing purposes
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    //Prints out the final result
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  }
}