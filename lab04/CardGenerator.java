/// By Rakene Chowdhury
/// Due on 09/19/2018
/// CSE 002 Section 210
/// This is a script that randomly generates a playing card.
//

//Creating a class called CardGenerator
public class CardGenerator {
  // main method required for every Java program
  public static void main (String[] args) {
    //Defining the variables
    int card = (int)(Math.random()*52)+1;
    //System.out.println(card);
    String suitString = "Basic";
    String idString = "Basic";
    
    //Using if statements to define suit
    if(card <= 13){
      suitString = "Diamonds";
    } else if (card > 13 && card <= 26){
      suitString = "Clubs";
    } else if (card > 26 && card <= 39){
      suitString = "Hearts";
    } else if (card > 39 && card <= 52){
      suitString = "Spades";
    } 
    
    //Using a switch statement to define card identity
    switch (card % 13){
      case 1:
        idString = "Ace";
        break;
      case 2:
        idString = "2";
        break;
      case 3:
        idString = "3";
        break;
      case 4:
        idString = "4";
        break;
       case 5:
        idString = "5";
        break;
       case 6:
        idString = "6";
        break;
       case 7:
        idString = "7";
        break;
       case 8:
        idString = "8";
        break;
       case 9:
        idString = "9";
        break;
       case 10:
        idString = "10";
        break;
       case 11:
        idString = "Jack";
        break;
       case 12:
        idString = "Queen";
        break;
       case 0:
        idString = "King";
        break;
      default:
        idString = "Error";
        break;
    }
    
    //Printing out the results
    System.out.println("You picked the " + idString + " of " + suitString);
    
  }
}